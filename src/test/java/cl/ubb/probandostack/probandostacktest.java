package cl.ubb.probandostack;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;

import org.junit.Test;

public class probandostacktest {

	@Test
	public void probandostack() {
		/*arrange*/
		probandostack stack = new probandostack();
	    boolean resultado;      
	    
	    /*act*/
	    resultado = stack.DeterminarStack(0);
	    
	    /*assert*/
	    assertThat(resultado,is(true));
	}

}
